// based off of https://app.pluralsight.com/player?course=quick-start-javascript-2-1917&author=susan-simkins&name=quick-start-javascript-2-1917-m2&clip=3&mode=live

// FOR loop

for(let i = 0; i < 11; i++) {
    console.log(i);
}
// below is the same as above
for(let i = 0; i <= 10; i++) {
    console.log(i);
}

/* 
DONT DO THIS. IT WILL CAUSE INFINITE LOOP
for (let i = 0; i >= 0; i++) {
    console.log(i);
}
*/



// DO WHILE loop

let i = 0; 
do {
    // first want to log variable inot the console
    // decalre vars before us them
    console.log(i);
    i++
} while(i <= 10);
// count from 0 and add 1 while it is still less than or equal to 10

/* 
this will be an infinte loop becdause nothing is changing the let (missing the i++)
let i = 0; 
do {
    console.log(i);
} while (i <= 10);
*/



// WHILE loop

// this is borrowing from the DO WHILE loop, but will need a let "i = 0" for WHILE loops as well
// while the condition in our parenthesis is true, do this 
while(i <= 10) {
    console.log(i);
    i++; 
}