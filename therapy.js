/*
a confirm pop up returns a boolean
if ok --> true
cancels --> false
 */

//  this does the confirm box that will either have ok or cancel
var sad = confirm("Would you like to be cheered up?");

//this is the same as saying while (sad === true)
while(sad) {
    alert("What is black and white and black and white and black and white? ......... A penguin rolling down a hill!");
    alert("What is black and white and laughing? ......... The penguin that pushed him!");
    
    let yes = confirm("Would you like to continue our session?");
    if(yes) {
        let response = prompt("Tell me about your troubles:", "Type your feelings here");

        if(response) {
            alert("I am sorry you are feeling down. I know you can't see it, but I will now give you a hug. **hugs**");
            sad = false;
        }
    } else {
        // no infinite loop witht his
        sad = false;
    }
}


/* 
another example:

let userLoggedIn = false;
while (userLoggedIn) {
    //display login info
}
*/