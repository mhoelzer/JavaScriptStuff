const headers = document.getElementsByClassName("really-big-heading")
const bigChiefHeading = document.getElementById("unique-even-bigger-heading")

// document.write("<h3 id='thing-i-just-wrote'>LEA IS THE BOMB THO</h3>")
console.log("Is this a string?", bigChiefHeading.innerHTML)

bigChiefHeading.innerHTML += "<h3 id='thing-i-just-wrote'>LEA IS THE BOMB THO</h3>" // this doesn't work because "headers" is a _collection_ of elements
// headers.innerHTML += "<h3 id='thing-i-just-wrote'>LEA IS THE BOMB THO</h3>" // this doesn't work because "headers" is a _collection_ of elements

const aside = document.createElement("aside")
aside.style.border = "1px solid black"
aside.style.padding = "20px"
const james = document.createElement("button")
james.textContent = "Cute as a button"
aside.appendChild(james)

bigChiefHeading.appendChild(aside)

console.log("headers:", headers)
console.log("bigChiefHeading:", bigChiefHeading)